import { observable, computed, toJS } from 'mobx';
import autobind from 'autobind-decorator';
import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';

const config = {
  apiKey: "AIzaSyAjx82U3H2NRpzgao_RHdJaJo91LFPzkGo",
  authDomain: "ginco-d2ed7.firebaseapp.com",
  databaseURL: "https://ginco-d2ed7.firebaseio.com",
  projectId: "ginco-d2ed7",
  storageBucket: "ginco-d2ed7.appspot.com",
  messagingSenderId: "319566687473"
};

var store = '';

/**
 * Classe resposável por prover o storage de informações que serão consumida pela aplicação
 */
@autobind
export default class Store {

  NUMBER_MAX_TEAM = 63;
  TEAM_ACCESS_PASS = "ooFeWbhC5FbFHIESIRvwiOH7cHO2";
  TEAM_ACCESS_USER = "teamaccess@team.com";
  IMAGE_SIZE = 2;

  /**
   * Variáveis observáveis
   */
  @observable user = '';
  @observable gincanas = [];
  @observable sessionDataGincanas = [];
  @observable error = '';
  @observable rules = '';
  @observable questionRules = '';
  @observable lang = 'PT_BR';
  @observable mailRecoverySend = '';
  @observable initReg = '';
  @observable mailVerificationSend = '';
  @observable actionVerify = false;
  @observable loading = false;
  @observable loadingUpdates = false;
  @observable feedBackAccountBlock = false;
  @observable gincanaElement = '';
  @observable gincanaTeams = '';
  @observable notifyFieldValueExists = '';
  @observable accesses = 0;
  @observable isTeacher = false;
  @observable hasImageUploadError = false;
  @observable team = "";
  @observable questions = [];
  @observable isValidEvent = false;
  @observable updatedQuestionSize = 0;
  @observable hasTeamUpdates = false;
  @observable jsonFileExport = {};
  @observable expireQuestionCadPeriod = false;

  //retorna as regras em um estado não observável
  @computed get dataRules() {
    return toJS(this.rules);
  }

  //retorna as regras em um estado não observável
  @computed get questionData() {
    return toJS(this.questions);
  }

  //retorna as regras em um estado não observável
  @computed get teamData() {
    return toJS(this.team);
  }

  //retorna as regras em um estado não observável
  @computed get gincanaElementData() {
    return toJS(this.gincanaElement);
  }

  //retorna as regras em um estado não observável
  @computed get dataQuestionRules() {
    return toJS(this.questionRules);
  }

  //realiza o get das Gincanas.
  @computed get dataGincanas() {
    return toJS(this.gincanas);
  }

  /**
   * construtor
   */
  constructor() {
    this.history = history;

    firebase.auth().onAuthStateChanged((firebaseUser) => {
      if (firebaseUser) {
        this.user = firebaseUser;
        this.isTeacher = firebaseUser != null ? firebaseUser.email != this.TEAM_ACCESS_USER : false
      } else {
        this.user = '';
      }
    });
  }

  /**
  * Método responsável por prover o singleton para o store
  */
  static getInstance() {
    if (store) return store;
    else {
      firebase.initializeApp(config);
      store = new Store();
      return store;
    }
  }

  /**
   * Recupera a sessão atual do firebase
   */
  getFirebaseInstance() {
    return firebase;
  }

  /**
   * Cria um ID unico para ser utilizado no sistema. 
   */
  getId() {
    function s4() {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }
    return s4() + s4() + '_' + s4() + '_' + s4() + '_' +
      s4() + '_' + s4() + s4() + s4();
  }
}