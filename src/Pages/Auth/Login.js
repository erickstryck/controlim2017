import React, { Component } from 'react';
import { Row, Col, Layout, Card, Form, Modal, notification, Spin, Avatar } from 'antd';
import '../../../public/css/login.css';
import FormLogin from './FormLogin';
import FormRecoveryPassword from './FormRecoveryPassword';
import autobind from 'autobind-decorator';
import { observer } from "mobx-react";
import DeathStar from 'react-deathStar';

const { Footer, Content } = Layout;

const FormLoginComponent = Form.create()(FormLogin);
const FormRecoveryPasswordComponent = Form.create()(FormRecoveryPassword);

import bookImage from '../../../public/images/book-8-bits.png';
import gplusIcon from '../../../public/icons/033-google-plus.svg';
import facebookIcon from '../../../public/icons/036-facebook.svg';
import authIcon from '../../../public/icons/id-card-2.svg';

/**
 * Componente Responsável por realizar o cadastro inicial do usuário
 * 
 */
@observer
@autobind
class Login extends Component {

  /**
   * Construtor da classe
   */
  constructor() {
    super();
    this.state = {
      modalVerifyAccount: false,
      deathStar: DeathStar.getInstance(React),
      modalPasswordRecovery: false,
      loading: false
    }
  }

  /**
   * Executado sempre que o componente acabar de ser atualizado.
   * 
   * @param {object} prevProps 
   * @param {object} prevState 
   */
  componentDidUpdate(prevProps, prevState) {
    this.modalVerifyAccount(prevProps.store);
    this.notifyError(prevProps.store.error);
    this.modalMailRecoverySuccess(prevProps.store.mailRecoverySend);
    this.modalMailVerificationSuccess(prevProps.store.mailVerificationSend);
    //Realiza a atualização do contexto deste componente para que se possa controlar o estado do mesmo em outro componente.
    prevState.deathStar.putStore("loginContext", this, true);
  }

  /**
   * Realiza as ações após a conclusão da montagem do componente
   */
  componentDidMount() {
    //Realiza a inserção do contexto deste componente para que se possa controlar o estado do mesmo em outro componente.
    this.state.deathStar.putStore("loginContext", this, true);
  }

  /**
   * Realiza as ações ao desmontar o componente
   */
  componentWillUnmount() {
    this.state.deathStar.destroy("loginContext");
  }

  /**
   * Exibe o erro do sistema com a mensagem especifica do problema.
   * 
   * @param {object} error 
   */
  notifyError(error) {
    if (error) {
      notification['error']({
        message: this.props.labels.LABEL_ERROR_TITLE_MODAL,
        description: error,
        onClose: () => { this.props.store.error = '' }
      });
    }
  };

  /**
   * Método responsável por realizar a ação de renvio de email
   */
  reSend() {
    this.props.centralCommand.executeAction('./Auth', 'sendEmailVerification');
  }

  /**
   * Método responsável por informar que a conta ainda não foi verificada e permitir um reenvio do email de verificação.
   * 
   * @param {object} store 
   */
  modalVerifyAccount(store) {
    if (store.user && !store.user.emailVerified && !this.state.modalVerifyAccount && this.props.store.feedBackAccountBlock) {
      this.props.store.feedBackAccountBlock = false;
      Modal.confirm({
        title: this.props.labels.LABEL_TITLE_MODAL_VERIFY_ACCOUNT,
        content: this.props.labels.LABEL_CONTENT_MODAL_VERIFY_ACCOUNT,
        okText: this.props.labels.LABEL_OK_TEXT_MODAL_VERIFY_ACCOUNT,
        cancelText: this.props.labels.LABEL_BACK_TEXT,
        onOk: this.reSend,
      });
    } else if (store.user && store.user.emailVerified && !store.finishRegister && store.actionVerify) {
      window.history.pushState(null,"",'/regCont');
      window.location.reload();
    }
  }

  /**
   * Função responsável por sinalizar em uma notificação se o e-mail de verificação foi reenviado com sucesso. 
   * 
   * @param {object} props 
   */
  modalMailVerificationSuccess(mailSend) {
    if (mailSend) {
      notification['success']({
        message: this.props.labels.LABEL_TITLE_MODAL_SEND_MAIL,
        description: this.props.labels.LABEL_CONTENT_MODAL_SEND_MAIL,
        onClose: () => { this.props.store.mailVerificationSend = false; }
      });
    }
  }

  /**
   * Função responsável por sinalizar em uma notificação se o e-mail de recuperação de senha foi reenviado com sucesso. 
   * 
   * @param {object} props 
   */
  modalMailRecoverySuccess(mailSend) {
    if (mailSend) {
      notification['success']({
        message: this.props.labels.LABEL_TITLE_MODAL_SEND_MAIL,
        description: this.props.labels.LABEL_CONTENT_MODAL_RECOVERY_MAIL,
        onClose: () => { this.props.store.mailRecoverySend = false; }
      });
    }
  }

  /**
   * Método responsável por abrir a rota de registro
   */
  toInit() {
    window.history.pushState(null,"",'/init');
    window.location.reload();
  }

  /**
   * Método responsável por abrir a rota de registro
   */
  resetPassword() {
    let modalFactoryContext = this.state.deathStar.getStore("modalFactoryContext");
    modalFactoryContext.state.submit = true;
    modalFactoryContext.update();
  }

  /**
   * Muda o estado de visualização do modal de recuperação de senha.
   */
  changeVisibleModalRecoveryPassword() {
    this.setState({
      modalPasswordRecovery: !this.state.modalPasswordRecovery
    });
    this.props.centralCommand.executeAction('./Auth', 'getRulesOfLogin');
  }

  /**
   * Efetua login utilizando a rede do google
   */
  googleLogin() {
    this.props.centralCommand.executeAction('./Auth', 'googleLogin');
  }

  /**
   * Efetua login utilizando a rede do facebook
   */
  facebookLogin() {
    this.props.centralCommand.executeAction('./Auth', 'facebookLogin');
  }

  /**
   * Método responsável por abrir a rota de registro
   */
  toRegister() {
    window.history.pushState(null,"",'/register');
    window.location.reload();
  }

  /**
   * Renderiza o componente
   */
  render() {
    //variveis observadas da store
    this.props.store.user;
    this.props.store.error;
    this.props.store.mailRecoverySend;
    this.props.store.mailVerificationSend;
    this.props.store.actionVerify;
    this.props.store.feedBackAccountBlock;
    this.props.store.loading;

    return (
      <Spin spinning={this.state.loading} tip={<span className='fontLoading'>{this.props.labels.LABEL_LOADING}</span>} size="large" >
        <Layout className='container' >
          <Modal />
          <Modal
            title={this.props.labels.LABEL_TITLE_MODAL_PASSWORD_RECOVERY}
            visible={this.state.modalPasswordRecovery}
            okText={this.props.labels.LABEL_PASSWORD_RECOVERY}
            cancelText={this.props.labels.LABEL_BACK_TEXT}
            onOk={this.resetPassword}
            onCancel={this.changeVisibleModalRecoveryPassword}
          >
            <FormRecoveryPasswordComponent {...this.props} />
          </Modal>
          <Content className='contentCad'>
            <Row type='flex' justify='center' className="cardCenter">
              <Col xl={9} lg={12} md={24} sm={32}>
                <Card>
                  <Spin spinning={this.props.store.loading} size="large" >
                    <Row type='flex' justify='center'>
                      <Col><span className="fontKarmaFuture logoText">{this.props.labels.LABEL_GINCO}</span><img src={bookImage} className="imgLogo" alt="logo" /></Col>
                    </Row>
                    <Row>
                      <Col><span className="title">{this.props.labels.LABEL_LOGIN_TITLE}</span></Col>
                    </Row>
                    <Row className='form'>
                      <Col span={24}>
                        <FormLoginComponent {...this.props} {...this.state} modalVerifyAccount={this.modalVerifyAccount} />
                      </Col>
                    </Row>
                  </Spin>
                  <Row className="subTitleRow">
                    <Col><span className="subTitle">{this.props.labels.LABEL_AUTH_SOCIAL}</span></Col>
                  </Row>
                  <Row className="socialIcons subTitle" type="flex" justify="center" align="center">
                    <Avatar className="pointer" onClick={this.googleLogin} shape="square" size="medium" src={gplusIcon}></Avatar>
                    <span>Google +</span>
                    <Avatar className="pointer" onClick={this.facebookLogin} shape="square" size="medium" src={facebookIcon} />
                    <span>Facebook</span>
                  </Row>
                  <Row className="hoverRow registerBorder">
                    <Col className="cardGuia pointer" onClick={this.toRegister}>
                      <Row type="flex" justify="space-around" align="middle">
                        <Col xl={1} lg={1} md={1} sm={24}>
                          <Avatar shape="square" size="large" src={authIcon} />
                        </Col>
                        <Col xl={12} lg={12} md={12} sm={24}>
                          <span className="title">{this.props.labels.LABEL_PATH_REGISTER}</span>
                        </Col>
                      </Row>
                    </Col>
                  </Row>
                </Card>
                <Row className="link-around">
                  <Col span={7}>
                    <span className="link pointer" onClick={this.changeVisibleModalRecoveryPassword}>{this.props.labels.LABEL_PASSWORD_RECOVER}</span>
                  </Col>
                  <Col span={1} offset={13}>
                    <span className="link pointer" onClick={this.toInit}>{this.props.labels.LABEL_INIT}</span>
                  </Col>
                </Row>
              </Col>
            </Row>
          </Content>
          <Footer className='footerCad' style={{ fontFamily: 'Lato Light', fontSize: '1.4em' }}>
            <span className="fontKarmaFuture" >{this.props.labels.LABEL_FOOTER}</span>
          </Footer>
        </Layout>
      </Spin>
    );
  }
}



export default Login;