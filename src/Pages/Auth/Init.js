import React, { Component } from 'react';
import { Row, Col, Layout, Card, Avatar } from 'antd';
import '../../../public/css/init.css';
import autobind from 'autobind-decorator';
import { observer } from "mobx-react";
import DeathStar from 'react-deathStar';

const { Footer, Content } = Layout;

import bookImage from '../../../public/images/book-8-bits.png';
import teachIcon from '../../../public/icons/professor.svg';
import teamIcon from '../../../public/icons/teamwork-1.svg';

/**
 * Componente Responsável por realizar o cadastro inicial do usuário
 * 
 */

@observer
@autobind
class Init extends Component {

  /**
   * Construtor da classe
   */
  constructor() {
    super();
    this.state = {
      deathStar: DeathStar.getInstance(React)
    }
  }

  /**
   * Realiza as ações após a conclusão da montagem do componente
   */
  componentDidMount() {
    //Realiza a inserção do contexto deste componente para que se possa controlar o estado do mesmo em outro componente.
    this.state.deathStar.putStore("loginContext", this, true);
  }

  /**
   * Realiza as ações ao desmontar o componente
   */
  componentWillUnmount() {
    this.state.deathStar.destroy("loginContext");
  }

  /**
   * Método responsável por abrir a rota de login
   */
  toLogin() {
    window.history.pushState(null,"",'/login');
    window.location.reload();
  }

  /**
   * Método responsável por abrir a rota de login
   */
  toTeamForm() {
    window.history.pushState(null,"",'/teams');
    window.location.reload();
  }

  /**
   * Renderiza o componente
   */
  render() {
    return (
      <Layout className='container' >
        <Content className='contentCad'>
          <Row type='flex' justify='center' className="cardCenter">
            <Col xl={9} lg={12} md={24} sm={32}>
              <Card>
                <Row type='flex' justify='center'>
                  <Col><span className="fontKarmaFuture logoText">{this.props.labels.LABEL_GINCO}</span><img src={bookImage} className="imgLogo" alt="logo" /></Col>
                </Row>
                <Row>
                  <Col><span className="title">{this.props.labels.LABEL_LOGIN_TITLE}</span></Col>
                </Row>
                <Row className="hoverRow">
                  <Col className="cardGuia pointer" onClick={this.toLogin}>
                    <Row type="flex" justify="space-around" align="middle">
                      <Col xl={1} lg={1} md={1} sm={24}>
                        <Avatar shape="square" size="large" src={teachIcon} />
                      </Col>
                      <Col xl={12} lg={12} md={12} sm={24}>
                        <span className="title">{this.props.labels.LABEL_PATH_TEACHER}</span>
                      </Col>
                    </Row>
                  </Col>
                </Row>
                <Row className="hoverRow">
                  <Col className="cardGuia pointer" onClick={this.toTeamForm}>
                    <Row type="flex" justify="space-around" align="middle">
                      <Col xl={1} lg={1} md={1} sm={24}>
                        <Avatar shape="square" size="large" src={teamIcon} />
                      </Col>
                      <Col xl={12} lg={12} md={12} sm={24}>
                        <span className="title">{this.props.labels.LABEL_PATH_TEAM}</span>
                      </Col>
                    </Row>
                  </Col>
                </Row>
              </Card>
            </Col>
          </Row>
        </Content>
        <Footer className='footerCad'>
          <span className="fontKarmaFuture" >{this.props.labels.LABEL_FOOTER}</span>
        </Footer>
      </Layout>
    );
  }
}



export default Init;