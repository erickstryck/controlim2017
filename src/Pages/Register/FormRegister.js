import React, { Component } from 'react';
import { Col, Layout, Form, Input, Button, Row } from 'antd';
import '../../../public/css/register.css';
import autobind from 'autobind-decorator';
import { observer } from 'mobx-react';

const FormItem = Form.Item;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 6 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 14 },
  },
};

/**
 * Componente Responsável por criar o formulário de cadastro
 * 
 */
@observer
@autobind
export default class FormRegister extends Component {

  /**
   * Construtor da classe
   */
  constructor() {
    super();
    this.state = {
      confirmDirty: false
    };
  }

  /**
   * Executado sempre que o componente está sendo montado
   */
  componentWillMount() {
    this.props.centralCommand.executeAction('./Auth', 'getRulesOfInitRegister');
  }

  /**
   *  Verifica se o campo é válido ao tirar o foco do mesmo e grava isso em um estado.
   * 
   * @param {event} e 
   */
  handleConfirmBlur(e) {
    const value = e.target.value;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  }

  /**
   * Verifica se as senhas informadas nos campos coincidem
   * 
   * @param {object} rule 
   * @param {object} value 
   * @param {object} callback 
   */
  checkPassword(rule, value, callback) {
    const form = this.props.form;
    if (value && value !== form.getFieldValue('password')) {
      callback(this.props.labels.LABEL_PASSWORDS_DO_NOT_MATCH);
    } else {
      callback();
    }
  }

  /**
   * Processo os dados do formulário para envio
   * 
   * @param {event} e 
   */
  handleForm(e) {
    e.preventDefault();
    this.props.store.loading = true;
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        this.props.centralCommand.executeAction('./Auth', 'initRegisterUser', [values.email, values.password]);
      }else{
        this.props.store.loading = false;
      }
    });
  }

  /**
   * Método responsável por realizar a ação de renvio de email
   */
  toLogin() {
    window.history.pushState(null,"",'/login');
    window.location.reload();
  }

  /**
   * Renderiza o componente
   */
  render() {
    const { getFieldDecorator } = this.props.form;
    const fontStyle = { fontFamily: 'Lato Regular', fontSize: '1.4em' };

    return (
      <Form onSubmit={this.handleForm}>
        <FormItem
          {...formItemLayout}
          label={<span style={fontStyle}>{this.props.labels.LABEL_EMAIL}</span>}
          hasFeedback
        >
          {this.props.store.dataRules ? getFieldDecorator('email', { rules: this.props.store.dataRules.emailRules })(<Input />) : ''}
        </FormItem>
        <FormItem
          {...formItemLayout}
          label={<span style={fontStyle}>{this.props.labels.LABEL_PASSWORD}</span>}
          hasFeedback
        >
          {this.props.store.dataRules ? getFieldDecorator('password', { rules: this.props.store.dataRules.passwordRules })(<Input type='password' />) : ''}
        </FormItem>
        <Row type='flex' justify='space-around' align='top'>
          <Button className='cadButton' style={{ fontFamily: 'Lato Regular', fontSize: '1.6em' }} htmlType='submit' size='large'>{this.props.labels.LABEL_INIT_REGISTER}</Button>
        </Row>
      </Form>
    );
  }
}