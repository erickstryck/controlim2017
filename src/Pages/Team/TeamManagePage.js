import React, { Component } from 'react';
import { Row, Col, Layout, Button, Modal, Card, Icon, Popconfirm, Tooltip, Avatar, Spin } from 'antd';
import autobind from 'autobind-decorator';
import CommonPage from '../Common/CommonPage';
import { observer } from "mobx-react";
import DeathStar from 'react-deathStar';
import '../../../public/css/teamPage.css';
import questionImg from '../../../public/images/question-img.png'
import validatingImg from '../../../public/icons/validating-img.svg';
import validImg from '../../../public/icons/valid-img.svg';
import noValidateImg from '../../../public/icons/noValidate-img.svg';

const { Header, Content } = Layout;

const confirm = Modal.confirm;
const { Meta } = Card;

/**
 * Componente Responsável por gerenciar as equipes
 */
@observer
@autobind
export default class TeamManagePage extends Component {

  /**
   * Construtor da classe
   */
  constructor() {
    super();
    this.state = {
      deathStar: DeathStar.getInstance(React),
      showModalCreateAccess: false,
      locationData: ''
    }
  }

  /**
   * Executado sempre que o componente acaba de ser montado.
   */
  componentDidMount() {
    if (this.state.locationData) {
      this.props.store.gincanaElement = window.history.state.gincana;
      this.props.centralCommand.executeAction('./Team', 'getTeamsOfEvent', [this.state.locationData.key, this.createTeamAccessCard]);
    }
  }

  /**
   * Criar um novo slote para que os times possam se cadastrar.
   */
  createAccessTeam() {
    this.handleModalCreateAccess();
    this.props.centralCommand.executeAction('./Team', 'saveAccessTeam', [this.state.locationData.key]);
  }

  /**
   * Exclui a equipe na base de dados.
   * 
   * @param {string} eventKey 
   * @param {string} teamKey
   * @param {number} keyView 
   */
  deleteTeam(keyView, eventKey, teamKey) {
    this.props.centralCommand.executeAction('./Team', 'deleteTeam', [keyView, eventKey, teamKey]);
  }

  /**
   * Abre a página de configurações da equipe
   */
  toTeamFormPage(eventKey, teamKey) {
    window.history.pushState({
      teamKeys: {
        eventKey: eventKey,
        teacherKey: this.props.store.user.uid,
        teamKey: teamKey
      }
    },"",'/teamform');
    window.location.reload();
  }

  /**
   * Cria um cartão com as informações de acesso das equipes.
   * 
   * @param {Object} teamValues 
   */
  createTeamAccessCard(teamValues, keys, reset, clear) {
    if (!this.state.deathStar.getElement('SlotArea') || reset || clear) {
      this.state.deathStar.processElement(<Row type='flex' justify='center' />, 'SlotArea', false, this);
    }

    let states = [
      <Tooltip placement="top" title={this.props.labels.LABEL_TEAM_VALIDATED}>
        <Avatar src={validImg} />
      </Tooltip>,
      <Tooltip placement="top" title={this.props.labels.LABEL_TEAM_VALIDATING}>
        <Avatar src={validatingImg} />
      </Tooltip>,
      <Tooltip placement="top" title={this.props.labels.LABEL_TEAM_NO_VALIDATE}>
        <Avatar src={noValidateImg} />
      </Tooltip>
    ];

    if (!clear) {
      this.state.deathStar.manipulate('SlotArea').setChildren(
        <Col style={{ margin: 10 }}>
          <Card
            style={{ width: 300 }}
            cover={<img alt="team" src={teamValues.image ? teamValues.image : questionImg} />}
            actions={[
              typeof teamValues.valid === "boolean" ? teamValues.valid ? states[0] : states[1] : states[2],
              <Tooltip placement="top" title={this.props.labels.LABEL_TEAM_CONFIGURE}>
                <Icon className="iconSizeCard" type="setting" onClick={this.toTeamFormPage.bind(this, keys.eventKey, keys.teamKey)} />
              </Tooltip>,
              <Popconfirm title={this.props.labels.LABEL_DELETE_TEAM_QUESTION} onConfirm={() => this.deleteTeam(keys.eventKey, keys.teamKey)}>
                <Tooltip placement="top" title={this.props.labels.LABEL_DEL}>
                  <Icon className="iconSizeCard" type="delete" />
                </Tooltip>
              </Popconfirm>
            ]}
          >
            <Meta
              title={teamValues.nome}
              description={<span style={{ fontWeight: 'bold', fontSize: '1.2em' }}>{this.props.labels.LABEL_TEAM_ACCESS_KEY + teamValues.accessPass}</span>}
            />
          </Card>
        </Col>
        , '', false, true);
    }
  }

  /**
   * Manipula a visualização do modal
   */
  handleModalCreateAccess() {
    this.setState({
      showModalCreateAccess: !this.state.showModalCreateAccess,
    });
  }

  /**
    * Renderiza o componente
    */
  render() {
    this.state.locationData = window.history.state.gincana;

    return (
      <CommonPage {...this.props} {...this.state}>
        {this.state.locationData ? <div>
          <Modal title={this.props.labels.LABEL_ADD_TEAM_ACESS}
            visible={this.state.showModalCreateAccess}
            onOk={this.createAccessTeam}
            confirmLoading={this.props.store.loadingUpdates}
            onCancel={this.handleModalCreateAccess}
            className={this.props.store.accesses >= this.props.store.NUMBER_MAX_TEAM ? 'teamLimit' : ''}
          >
            {this.props.store.accesses < this.props.store.NUMBER_MAX_TEAM ? <Row type='flex' justify='center' style={{ fontSize: '1.5em' }}>
              <Row type='flex' justify='center'><span>{this.props.labels.LABEL_ADD_TEAM_ACESS_NUMBER + (this.props.store.NUMBER_MAX_TEAM - this.props.store.accesses)}</span></Row>
            </Row> : <Row type='flex' justify='center' style={{ fontSize: '1.5em', color: 'red' }} >
                <Row type='flex' justify='center'><span>{this.props.labels.LABEL_TEAM_LIMIT_REACHED}</span></Row>
              </Row>}
          </Modal>
          <Layout>
            <Header className='header'>
              <Row type='flex' justify='center'>
                <Col>
                  <span className="homeTitle">{this.props.labels.LABEL_MANAGE_TEAM}</span>
                </Col>
              </Row>
              <Row type='flex' justify='center'>
                <Col>
                  <span className="homeTitle">{this.props.labels.LABEL_EVENT + " : " + this.state.locationData.nome}</span>
                </Col>
              </Row>
              <Row type='flex' justify='center'>
                <Col>
                  <span className="homeTitle">{this.props.labels.LABEL_THEME + " : " + this.state.locationData.tema}</span>
                </Col>
              </Row>
            </Header>
            <Content>
              {this.state.deathStar.getElement('SlotArea')}
              <Row type='flex' justify='center'>
                <Button className="buttonSearch" onClick={this.handleModalCreateAccess} size="large">{this.props.labels.LABEL_ADD_TEAM}</Button>
              </Row>
            </Content>
          </Layout>
        </div> : <Spin spinning={true} size="large" ></Spin>}
      </CommonPage>
    );
  }
}
