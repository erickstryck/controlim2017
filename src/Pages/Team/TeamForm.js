import React, { Component } from 'react';
import { Form, Input, Button, Row, Icon } from 'antd';
import '../../../public/css/register.css';
import autobind from 'autobind-decorator';
import { observer } from "mobx-react";
import DeathStar from 'react-deathStar';

const FormItem = Form.Item;
/**
 * Componente Responsável por criar o formulário de cadastro
 * 
 */
@observer
@autobind
class TeamForm extends Component {

  /**
   * Construtor da classe
   */
  constructor() {
    super();
    this.state = {
      deathStar: DeathStar.getInstance(React),
      confirmDirty: false,
      build: false
    };
  }

  /**
   * Realiza as ações quando se monta o componente
   */
  componentDidMount() {
    this.props.centralCommand.executeAction('./Team', 'getRulesOfTeam', [this.checkConfirm]);
  }

  /**
   * Método responsável por verficar se os campos informados estão corretamente preenchidos.
   * 
   * @param {object} rule 
   * @param {object} value 
   * @param {object} callback 
   */
  checkConfirm(rule, value, callback) {
    const form = this.props.form;
    this.props.store.hasTeamUpdates = true;
    if (value && this.state.confirmDirty) {
      form.validateFields(['confirm'], { force: true });
    }
    callback();
  }

  /**
   *  Verifica se o campo é válido ao tirar o foco do mesmo e grava isso em um estado.
   * 
   * @param {event} e 
   */
  handleConfirmBlur(e) {
    const value = e.target.value;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  }

  /**
   * Processo os dados do formulário para envio
   * 
   * @param {event} e 
   */
  handleForm(e) {
    e.preventDefault();
    
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        this.props.centralCommand.executeAction('./Team', 'update', [values]);
      }
    });
  }

  /**
   * Valida se alguma equipe já possui o nome informado
   * 
   * @param {object} element 
   */
  verifyIfTeamNameExist(element) {
    this.props.centralCommand.executeAction('./Team', 'verifyIfTeamNameExist', [element.target.value]);
  }

  /**
   * Remove um campo relacionado ao aluno
   * 
   * @param {number} index 
   */
  removeStudent(index) {
    const { form } = this.props;
    const keys = form.getFieldValue('keys');
    if (keys.length === 1) {
      return;
    }
    form.setFieldsValue({
      keys: keys.filter((key, idx) => idx !== index),
    });

    this.props.store.hasTeamUpdates = true;
  }

  /**
   * Adiciona um novo campo para aluno
   */
  addStudent() {
    const { form } = this.props;
    const keys = form.getFieldValue('keys');
    const nextKeys = keys.concat(++keys.length);
    form.setFieldsValue({
      keys: nextKeys,
    });

    this.props.store.hasTeamUpdates = true;
  }

  /**
   * Retorna os campos para cadastro de alunos
   */
  getStudentFileds() {
    const { getFieldDecorator, getFieldValue } = this.props.form;
    getFieldDecorator('keys', { initialValue: this.props.store.teamData.alunos ? this.props.store.teamData.alunos : [] });
    const keys = getFieldValue('keys');
    return keys.map((element, index) => {
      return (
        <FormItem
          label={<span style={{ fontFamily: 'Lato Regular', fontSize: '1.4em' }}>{this.props.labels.LABEL_STUDENT}</span>}
          required={false}
          key={index}
        >
          {getFieldDecorator(`alunos[${index}]`, {
            validateTrigger: ['onChange', 'onBlur'],
            rules: this.props.store.dataRules.student,
            initialValue: this.props.store.teamData.alunos ? this.props.store.teamData.alunos[index] : ''
          })(
            <Input placeholder={this.props.labels.LABEL_STUDENT_NAME} style={{ width: '95%', marginRight: 8 }} />
          )}
          {keys.length > 1 ? (
            <Icon
              className="dynamic-delete-button"
              type="minus-circle-o"
              disabled={keys.length === 1}
              onClick={() => this.removeStudent(index)}
            />
          ) : null}
        </FormItem>
      );
    });
  }

  /**
   * Renderiza o componente
   */
  render() {
    const { getFieldDecorator } = this.props.form;
    const fontStyle = { fontFamily: 'Lato Regular', fontSize: '1.4em' };
    let notifyStatus = this.props.store.notifyFieldValueExists;

    return (
      <Form onSubmit={this.handleForm}>
        <FormItem
          label={<span style={fontStyle}>{this.props.labels.LABEL_TEAM_NAME}</span>}
          validateStatus={notifyStatus.validateStatus}
          help={notifyStatus.help}
          hasFeedback
        >
          {this.props.store.dataRules.nameRules ? getFieldDecorator('nome', { rules: this.props.store.dataRules.nameRules, initialValue: this.props.store.team.nome })(<Input onBlur={this.verifyIfTeamNameExist} />) : ''}
        </FormItem>
        <FormItem
          label={<span style={fontStyle}>{this.props.labels.LABEL_WARCRY}</span>}
          hasFeedback>
          {this.props.store.dataRules.warCry ? getFieldDecorator('message', { rules: this.props.store.dataRules.warCry, initialValue: this.props.store.team.message })(<Input />) : ''}
        </FormItem>
        {this.props.store.teamData ? this.getStudentFileds() : <div />}
        <FormItem>
          <Button type="dashed" onClick={this.addStudent} style={{ width: '60%' }}>
            <Icon type="plus" /> {this.props.labels.LABEL_ADD_STUDENT}
          </Button>
        </FormItem>
        <Row type="flex" justify="end" align="middle">
          <Button className='cadButton' style={{ fontFamily: 'Lato Regular', fontSize: '1.2em', marginTop: 40, display: this.props.store.hasTeamUpdates && !this.props.store.notifyFieldValueExists && !this.props.store.expireQuestionCadPeriod ? 'block' : 'none' }} htmlType="submit" size="large">{this.props.labels.LABEL_UPDATE_TEAM}</Button>
        </Row>
      </Form>
    );
  }
}

export default TeamForm;