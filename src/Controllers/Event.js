import Constants from '../Util/Constants';
import Store from '../Util/Store';
import moment from 'moment';
import CentralCommand from './CentralCommand';

var firebase;
/**
 * Classe responsável por prover os serviços de authenticação
 */
export default class Event {

  /**
   * Construtor da classe
   * 
   * @param {object} firebase 
   */
  constructor(firebaseContext) {
    firebase = firebaseContext;
    this.store = Store.getInstance();
  }

  /**
   * Recupera as regras dos campos para o formulário de criação de gincana.
   * 
   * @param {object} checkConfirm 
   */
  getRulesOfNewEvent(checkConfirm) {
    let rules = {};
    let messages = Constants[this.store.lang];

    rules.nameRules = [{
      type: 'string', message: messages.LABEL_REQUIRED
    }, {
      required: true, message: messages.LABEL_PLEASE_INSERT_NAME
    }, {
      validator: checkConfirm
    }, {
      min: 6, message: messages.MSG_WEAK_NAME
    }, {
      max: 40, message: messages.MSG_WEAK_NAME
    }];


    rules.teacherRules = [{
      type: 'string', message: messages.LABEL_REQUIRED
    }, {
      required: true, message: messages.LABEL_PLEASE_INSERT_NAME
    }, {
      validator: checkConfirm
    }, {
      min: 6, message: messages.MSG_WEAK_NAME
    }, {
      max: 40, message: messages.MSG_WEAK_NAME
    }];

    rules.timeRules = [{
      type: 'number', message: messages.LABEL_REQUIRED
    }, {
      required: true, message: messages.LABEL_PLEASE_INSERT_TIME
    }, {
      validator: checkConfirm
    }];

    rules.themeRules = [{
      type: 'string', message: messages.LABEL_REQUIRED
    }, {
      required: true, message: messages.LABEL_PLEASE_INSERT_THEME
    }, {
      validator: checkConfirm
    }, {
      min: 6, message: messages.MSG_WEAK_THEME_NAME
    }, {
      max: 40, message: messages.MSG_WEAK_THEME_NAME
    }];

    rules.dataEventRules = [{
      type: 'object', message: messages.LABEL_REQUIRED
    }, {
      required: true, message: messages.LABEL_PLEASE_INSERT_DATE
    }];

    rules.dataLimitEventRules = [{
      type: 'object', message: messages.LABEL_REQUIRED
    }, {
      required: true, message: messages.LABEL_PLEASE_INSERT_DATE
    }];

    this.store.rules = rules;
  }

  /**
   * Recupera a inforamção se o evento é válido para importação ou não
   * 
   * @param {string} eventKey 
   */
  isValidEvent(eventKey) {
    this.store.loading = true;
    firebase.database().ref('/professores/' + this.store.user.uid + '/gincanas/' + eventKey).once('value', (snapshot) => {
      let dataGincanas = snapshot.val();
      let valid = true;
      let teamCount = 0;
      if (dataGincanas) {
        Object.keys(dataGincanas.equipes).every(key => {
          teamCount++;
          let equipe = dataGincanas.equipes[key];
          if (!equipe.valid) {
            valid = false;
            return false;
          }

          return true;
        });

        valid = valid && teamCount >= 2;
      }

      Store.getInstance().isValidEvent = valid;
      Store.getInstance().loading = false;
    });
  }

  /**
   * Verifica se já existe uma gincana cadastrada com o nome informado.
   */
  verifyIfEventNameExist(name) {
    const locationState = window.history.state;
    const dataAtualizacao = locationState ? locationState.gincanaAtualizacao : '';

    if (dataAtualizacao.nome != name) {
      this.store.loading = true;
      return firebase.database().ref('/professores/' + this.store.user.uid + '/gincanas').once('value', (snapshot) => {
        let dataGincanas = snapshot.val();
        if (dataGincanas) {
          dataGincanas = Object.values(dataGincanas);
          let filtered = dataGincanas.filter(gincana => {
            return gincana.nome == name;
          });

          let messages = Constants[Store.getInstance().lang];
          if (filtered.length) {
            Store.getInstance().notifyFieldValueExists = { validateStatus: "error", help: messages.LABEL_EVENT_NAME_EXIST };
          } else {
            Store.getInstance().notifyFieldValueExists = ''
          }
        }
        Store.getInstance().loading = false;
      });
    }
  }

  /**
   * Processo o arquivo e disponibiliza o objeto json para download.
   * 
   * @param {function} callback 
   */
  processFileJson(eventKey, callback) {
    this.store.loading = true;
    firebase.database().ref('/professores/' + this.store.user.uid + '/gincanas/' + eventKey).once('value', (snapshot) => {
      let dataGincanas = snapshot.val();
      if (dataGincanas) {
        let objJson = {};

        objJson.nome = dataGincanas.nome;
        objJson.tema = dataGincanas.tema;
        objJson.time = dataGincanas.time;
        objJson.professor = dataGincanas.professor;
        objJson.equipes = [];

        for (let key in dataGincanas.equipes) {
          let equipe = {};

          equipe.nome = dataGincanas.equipes[key].nome;
          equipe.message = dataGincanas.equipes[key].message;
          equipe.image = dataGincanas.equipes[key].image;
          equipe.alunos = dataGincanas.equipes[key].alunos.filter(Boolean).map(aluno => {
            return { nome: aluno }
          });
          equipe.perguntas = [];

          for (let keyPergunta in dataGincanas.equipes[key].perguntas) {
            let pergunta = dataGincanas.equipes[key].perguntas[keyPergunta];
            let objPergunta = {};

            objPergunta.enunciado = pergunta.enunciado;
            objPergunta.alternativas = pergunta.alternativas;

            equipe.perguntas.push(objPergunta);
          }

          objJson.equipes.push(equipe);
        }

        callback({ name: dataGincanas.nome + "-" + dataGincanas.tema + "-" + dataGincanas.dataRealizacao + ".json", value: { gincana: objJson } });
        Store.getInstance().loading = false;
      } else {
        Store.getInstance().loading = false;
      }
    });
  }

  /**
   * Salva os dados da gincana na base de dados.
   * 
   * @param {object} values 
   */
  saveEvent(values) {
    this.store.loading = true;
    this.validateEventData(values);
    values.dataRealizacao = values.dataRealizacao.format("DD/MM/YYYY");
    values.dataLimitePerguntas = values.dataLimitePerguntas.format("DD/MM/YYYY");
    delete values.key;

    if (!this.store.notifyFieldValueExists) {
      firebase.database().ref('/professores/' + this.store.user.uid + '/gincanas').push(values).then(function (result) {
        Store.getInstance().loading = false;
        values.key = result.key;
        window.history.pushState({ gincana: values }, "", '/team');
        window.location.reload();
      }).catch(function (error) {
        Store.getInstance().loading = false;
        CentralCommand.processError(error);
      });
    } else {
      this.store.loading = false;
    }
  }

  /**
   * Responsável por validar as datas da gincana
   */
  validateEventData(values) {
    values.dataLimitePerguntas.set({ hour: 23, minute: 59, second: 0, millisecond: 0 });
    values.dataRealizacao.set({ hour: 23, minute: 59, second: 59, millisecond: 0 });

    let isafter = values.dataRealizacao.isAfter(values.dataLimitePerguntas) && values.dataLimitePerguntas.isAfter(moment());
    if (!isafter) {
      Store.getInstance().loading = false;
      throw { code: Constants.DATE_EVENT_INVALID };
    }
  }

  /**
   * Atualiza a gincana na base de dados
   * 
   * @param {object} values 
   */
  updateEvent(values) {
    this.store.loading = true;
    this.validateEventData(values);
    values.dataRealizacao = values.dataRealizacao.format("DD/MM/YYYY");
    values.dataLimitePerguntas = values.dataLimitePerguntas.format("DD/MM/YYYY");

    if (!this.store.notifyFieldValueExists) {
      firebase.database().ref('/professores/' + this.store.user.uid + '/gincanas/' + values.key).update(values).then(function (result) {
        Store.getInstance().loading = false;
        Store.getInstance().gincanaElement = result;
      }).catch(function (error) {
        Store.getInstance().loading = false;
        CentralCommand.processError(error);
      });
    } else {
      this.store.loading = false;
    }
  }

  /**
   * Exclui a gincana na base de dados.
   * 
   * @param {object} values 
   */
  deleteEvent(values) {
    this.store.loading = true;
    firebase.database().ref('/teamAccess').once('value', (access) => {
      access = access.val();
      if (access) {
        let revokeAccess = Object.keys(access).filter(key => {
          return access[key].eventKey == values.key;
        });

        revokeAccess.forEach((keyTarget) => {
          firebase.database().ref('/teamAccess/' + keyTarget).remove();
        });
        firebase.database().ref('/professores/' + this.store.user.uid + '/gincanas/' + values.key).remove();
        Store.getInstance().loading = false;
      }
    });
  }

  /**
   * Recupera as gincanas da base de dados
   */
  getEvent() {
    this.store.loading = true;
    return firebase.database().ref('/professores/' + this.store.user.uid + '/gincanas').on('value', (snapshot) => {
      let dataGincanas = snapshot.val();
      if (dataGincanas) {
        let gincanasStore = [];
        let count = 0;
        for (var key in dataGincanas) {
          dataGincanas[key]['rowKey'] = count;
          dataGincanas[key]['key'] = key;
          gincanasStore.push(dataGincanas[key]);
          count++;
        }
        Store.getInstance().gincanas = gincanasStore;
      }
      Store.getInstance().loading = false;
    });
  }
}