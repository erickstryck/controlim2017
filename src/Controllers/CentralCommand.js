import Store from "../Util/Store";
import Constants from "../Util/Constants";

var instance;
var firebase;
/**
 * Node Reflexivo Voltado a Iteração com o Sistema
 */
export default class CentralCommand {
  /**
   * Função responsável por recuperar a instância singleton de CentralCommand e passar via construtor o firebase para a Store e itens instanciados dinamicamente.
   */
  static getInstance() {
    if (instance) {
      return instance;
    } else {
      instance = new CentralCommand();
      let store = Store.getInstance();
      firebase = store.getFirebaseInstance();
      return instance;
    }
  }
  /**
   * Executa a ação requisitada importando automáticamente os itens no controller
   *
   * @param {string} target
   * @param {string} method
   * @param {array} param
   * @param {array} construct
   */
  executeAction(target, method, param = [], construct = []) {
    construct.push(firebase);
    return this.process(target, method, param, construct);
  }

  /**
   * Método responsável por executar a reflexão encima dos parametro informados
   *
   * @param {string} target
   * @param {string} method
   * @param {array} param
   * @param {array} construct
   */
  process(target, method, param = [], construct = []) {
    return require.ensure([], function() {
      try {
        let invoke = require("" + target); // a expressão ''+ é usada para fazer um casting explicito para string no js

        let classe = Reflect.construct(invoke.default, construct);
        if (Reflect.has(classe, method)) {
          return Reflect.apply(classe[method], classe, param);
        } else {
          throw {
            code: Constants.CENTRAL_COMMAND,
            message: "O método " + method + " não foi encontrado!"
          };
        }
      } catch (error) {
        console.error(error);
        CentralCommand.processError(error);
      }
    });
  }

  /**
   * Método resposável por identificar e notificar o usuário sobre uma exceção
   *
   * @param {object} error
   */
  static processError(error) {
    let store = Store.getInstance();
    let messages = Constants[store.lang];

    switch (error.code) {
      case Constants.WRONG_PASSWORD:
        store.error = messages.MSG_INVALID_PASSWORD;
        break;
      case Constants.USER_NOT_FOUND:
        store.error = messages.MSG_INVALID_USER;
        break;
      case Constants.EMAIL_IN_USE:
        store.error = messages.LABEL_INFORMED_EMAIL_IS_ALREADY_BEING_USED;
        break;
      case Constants.EMAIL_RESEND_FAILURE:
        store.error = messages.LABEL_CANT_SEND_MAIL;
        break;
      case Constants.WEAK_PASSWORD:
        store.error = messages.MSG_WEAK_PASSWORD;
        break;
      case Constants.REMOTE_ARCHIVE_FAIL:
        store.error = messages.MSG_REMOTE_ARCHIVE_FAIL;
        break;
      case Constants.DATE_EVENT_INVALID:
        store.error = messages.MSG_DATE_EVENT_INVALID;
        break;
      case Constants.INVALID_ACCESS_KEY:
        store.error = messages.LABEL_INVALID_ACCESS_KEY;
        break;
      default:
        store.error = messages.MSG_FATAL_ERROR;
        setTimeout(() => {
          if (store.isTeacher) {
            window.history.pushState(null, "", "/home");
          } else {
            window.history.pushState(null, "", "/init");
          }
          window.location.reload();
        }, 5000);
        break;
    }

    store.loading = false;
  }
}
